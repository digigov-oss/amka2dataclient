import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export declare type AuditInit = AuditRecord;
export declare type amka2dataOutputRecord = {
    aaInput: string;
    amkaInput: string;
    amkaCurrent: string;
    lastDataLogDate: string;
    adt: string;
    nationalityCountry: string;
    nationalityCountryCode: string;
    afm: string;
    birthSurnameEl: string;
    idSurnameEl: string;
    firstNameEl: string;
    fatherNameEl: string;
    motherNameEl: string;
    birthSurnameEn: string;
    idSurnameEn: string;
    firstNameEn: string;
    fatherNameEn: string;
    motherNameEn: string;
    birthDate: string;
    fictitiousBirthDateIndication: string;
    deathDate: string;
    callSequenceId: string;
    callSequenceDate: string;
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns amka2dataOutputRecord | ErrorRecord
 */
export declare const getData: (amka: string, user: string, pass: string, overrides?: overrides | undefined) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    aaInput: string;
    amkaInput: string;
    amkaCurrent: string;
    lastDataLogDate: string;
    adt: string;
    nationalityCountry: string;
    nationalityCountryCode: string;
    afm: string;
    birthSurnameEl: string;
    idSurnameEl: string;
    firstNameEl: string;
    fatherNameEl: string;
    motherNameEl: string;
    birthSurnameEn: string;
    idSurnameEn: string;
    firstNameEn: string;
    fatherNameEn: string;
    motherNameEn: string;
    birthDate: string;
    fictitiousBirthDateIndication: string;
    deathDate: string;
    callSequenceId: string;
    callSequenceDate: string;
} | {
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    errorCode: string;
    errorDescr: string;
}>;
export default getData;
