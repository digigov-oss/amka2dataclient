"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getData = void 0;
const soapClient_js_1 = __importDefault(require("./soapClient.js"));
const gsis_audit_record_db_1 = require("@digigov-oss/gsis-audit-record-db");
const config_json_1 = __importDefault(require("./config.json"));
/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns amka2dataOutputRecord | ErrorRecord
 */
const getData = (amka, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    const endpoint = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _a !== void 0 ? _a : "";
    const prod = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _b !== void 0 ? _b : false;
    const auditInit = (_c = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _c !== void 0 ? _c : {};
    const auditStoragePath = (_d = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _d !== void 0 ? _d : "/tmp";
    const auditEngine = (_e = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _e !== void 0 ? _e : new gsis_audit_record_db_1.FileEngine(auditStoragePath);
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const response = yield s.getData(amka, auditRecord.auditTransactionId || '');
        return Object.assign(Object.assign({}, response), auditRecord);
    }
    catch (error) {
        throw (error);
    }
});
exports.getData = getData;
exports.default = exports.getData;
