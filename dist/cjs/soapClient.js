"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const soap_1 = __importDefault(require("soap"));
let soap = soap_1.default;
try {
    soap = require('soap');
}
catch (error) {
    //my hackish way to make soap work on both esm and cjs
    //theshoap on esm is undefined
    //On esm require is not defined, however on cjs require can be used.
    //So we try to use require and if it fails we use the thesoap module
}
/**
 * SOAP client for getNncIdentity
 * @class Soap
 * @description SOAP client for getNncIdentity
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
class Soap {
    constructor(wsdl, username, password, auditRecord, endpoint) {
        this._wsdl = wsdl;
        this._username = username;
        this._password = password;
        this._auditRecord = auditRecord;
        this._endpoint = endpoint;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const client = yield soap.createClientAsync(this._wsdl, {
                    wsdl_headers: {
                        'Authorization': 'Basic ' + Buffer.from(`${this._username}:${this._password}`).toString('base64'),
                    },
                });
                if (this._endpoint) {
                    client.setEndpoint(this._endpoint);
                }
                return client;
            }
            catch (e) {
                throw e;
            }
        });
    }
    getData(amka, aa) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const client = yield this.init();
                var options = {
                    hasNonce: true,
                    actor: 'actor'
                };
                var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
                client.setSecurity(wsSecurity);
                const auditRecord = this._auditRecord;
                const args = {
                    auditRecord: auditRecord,
                    amka2dataInputRecord: {
                        aa: aa,
                        amka: amka
                    }
                };
                const result = yield client.amka2dataAsync(args);
                const errorRecord = result[0].errorRecord;
                if (errorRecord) {
                    return errorRecord;
                }
                else {
                    return result[0].amka2dataOutputRecord;
                }
            }
            catch (e) {
                throw e;
            }
        });
    }
}
exports.default = Soap;
