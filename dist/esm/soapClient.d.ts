import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
/**
 * SOAP client for getNncIdentity
 * @class Soap
 * @description SOAP client for getNncIdentity
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
declare class Soap {
    private _wsdl;
    private _username;
    private _password;
    private _auditRecord;
    private _endpoint;
    constructor(wsdl: string, username: string, password: string, auditRecord: AuditRecord, endpoint: string);
    init(): Promise<any>;
    getData(amka: string, aa: string): Promise<any>;
}
export default Soap;
