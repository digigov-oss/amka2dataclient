import soapClient from './soapClient.js';
import {generateAuditRecord, AuditRecord, FileEngine, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 


export declare type AuditInit = AuditRecord;
export declare type amka2dataOutputRecord = {
    aaInput: string; 
    amkaInput: string;
    amkaCurrent: string; 
    lastDataLogDate: string;
    adt: string; 
    nationalityCountry: string;
    nationalityCountryCode: string; 
    afm: string;
    birthSurnameEl: string; 
    idSurnameEl: string; 
    firstNameEl: string; 
    fatherNameEl: string; 
    motherNameEl: string;  
    birthSurnameEn: string; 
    idSurnameEn: string; 
    firstNameEn:string; 
    fatherNameEn:string; 
    motherNameEn: string; 
    birthDate:string; 
    fictitiousBirthDateIndication: string; 
    deathDate: string; 
    callSequenceId: string; 
    callSequenceDate:string; 
}

export declare type ErrorRecord = {
    errorCode:string;
    errorDescr:string;
}

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
   endpoint?:string;
   prod?:boolean;
   auditInit?: AuditRecord;
   auditStoragePath?: string;
   auditEngine?: AuditEngine;
}

/**
 * 
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns amka2dataOutputRecord | ErrorRecord
 */
export const getData = async (amka:string, user:string, pass:string, overrides?:overrides | undefined) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp"
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');
  
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const response:
        | amka2dataOutputRecord
        | ErrorRecord = await s.getData(amka, auditRecord.auditTransactionId || '');
        return {...response,...auditRecord};
       } catch (error) {
           throw(error);
       }

}
export default getData;