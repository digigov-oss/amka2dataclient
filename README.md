# amka2dataClient

Client to connect on IDIKA service, Returns personal info.

#### Example:

```
import getData from '@digigov-oss/amka2data-client';
import config from './config.json'; 
const test = async () => {
    try {
        const data = await getData(config.amka, config.user, config.pass);
        return data;
    } catch (error) {
        console.log(error);
    }
}

test().then((amka) => { console.log('amka2dataOutputRecord',amka); });
```

* you can use `overrides` to override the default values
* for your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at the test folder, for example.

Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

#### Returns
an object like the following:
```
amka2dataOutputRecord {
  amka2dataOutputRecord: {
    aaInput: '73',
    amkaInput: 'XXXXXXXX',
    amkaCurrent: 'XXXXXXXX',
    lastDataLogDate: '27/11/2017',
    adtType: 'Τ',
    adt: 'XXXXXXXX',
    yearOfAdtPublished: '1992',
    nationalityCountry: 'ΕΛΛΑΔΑ',
    nationalityCountryCode: 'GR',
    sex: 'ΑΡΡΕΝ',
    afm: 'XXXXXXXX',
    birthSurnameEl: 'ΣΚΑΡΒΕΛΗΣ',
    idSurnameEl: 'ΣΚΑΡΒΕΛΗΣ',
    firstNameEl: 'ΠΑΝΑΓΙΩΤΗΣ',
    fatherNameEl: 'XXXXXXXX',
    motherNameEl: 'XXXXXXXX',
    birthSurnameEn: 'SKARVELIS',
    idSurnameEn: 'SKARVELIS',
    firstNameEn: 'PANAGIOTIS',
    fatherNameEn: 'XXXXXXXX',
    motherNameEn: 'XXXXXXXX',
    birthDate: 'XXXXXXXX',
    fictitiousBirthDateIndication: '',
    birthCountry: 'ΕΛΛΑΔΑ',
    birthCountryCode: 'GR',
    birthMunicipality: 'ΒΥΡΩΝΑ',
    birthCountyCode: 'ΑΤΤΙ',
    addressStreetNum: 'XXXXXXXX',
    addressCity: 'XXXXXXXX',
    addressCountyCode: 'ΑΤΤΙ',
    addressPostalCode: '',
    addressCountry: 'ΕΛΛΑΔΑ',
    addressCountryCode: 'GR',
    phone1: 'XXXXXXXX'
  },
  callSequenceId: 48185216,
  callSequenceDate: 2022-02-17T11:21:50.298Z,
  errorRecord: null,
  auditUnit: 'gov.gr',
  auditTransactionId: '73',
  auditProtocol: '38/2022-02-17',
  auditTransactionDate: '2022-02-17T11:21:49Z',
  auditUserIp: '127.0.0.1',
  auditUserId: 'system'
}
```
or an error object like the following:
```
amka2dataOutputRecord {
  amka2dataOutputRecord: { aaInput: '74', amkaInput: 'XXXXXXXX' },
  callSequenceId: 48149867,
  callSequenceDate: 2022-02-17T11:26:35.513Z,
  errorRecord: { errorCode: 'WS_EXTERNAL_ERROR', errorDescr: 'ΛΑΘΟΣ ΣΤΟΙΧΕΙΩΝ' },
  auditUnit: 'gov.gr',
  auditTransactionId: '74',
  auditProtocol: '39/2022-02-17',
  auditTransactionDate: '2022-02-17T11:26:35Z',
  auditUserIp: '127.0.0.1',
  auditUserId: 'system'
}
```

#### note
To test it, you have to use your real amka number.

#### * known issues
KED advertises a wrong endpoint(!) for the `amka2data` service on production WSDL. So, you have to use (override) the endpoint: `https://ked.gsis.gr/esb/amkaInfoService`
You can do that by setting the `endpoint` property on the `overrides` object.

```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/amkaInfoService',
}
```
